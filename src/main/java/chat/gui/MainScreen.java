package chat.gui;

import network.ListnerClass;
import network.MessageTransmitter;
import network.WritableGUI;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class MainScreen extends javax.swing.JFrame implements WritableGUI {
    ListnerClass listnerClass;
    MessageTransmitter messageTransmitter;
    private JTextField targetPortTextField;
    private JTextField ipTextField;
    private JTextArea chat;
    private JTextField messageField;
    private JButton sendButton;
    private JButton listenButton;
    private JTextField recieverPort;

    public void write(String s) {
        chat.append(s + "\n");
    }

    public void setListenButtonActionPerformed(ActionEvent actionEvent) {
        listnerClass = new ListnerClass(this, Integer.parseInt(recieverPort.getText()));
        listnerClass.start();

    }

    public void setSendButton(ActionEvent actionEvent) {
        messageTransmitter = new MessageTransmitter(messageField.getText(), ipTextField.getText(), Integer.parseInt
                (targetPortTextField.getText()));
        messageTransmitter.start();

    }
}
