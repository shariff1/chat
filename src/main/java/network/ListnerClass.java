package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ListnerClass extends Thread {
    ServerSocket serverSocket;
    int port = 8877;
    WritableGUI writableGUI;

    public ListnerClass(WritableGUI writableGUI,
                        int port) {
        this.writableGUI = writableGUI;
        this.port = port;
        try {
            serverSocket = new ServerSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ListnerClass() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        Socket clientSocket;
        try {
            while ((clientSocket = serverSocket.accept()) != null) {
                InputStream inputStream = clientSocket.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = bufferedReader.readLine();
                if (line != null) {
                    writableGUI.write(line);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
