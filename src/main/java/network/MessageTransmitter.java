package network;

import java.io.IOException;
import java.net.Socket;

public class MessageTransmitter extends Thread {
    private String hostname, message;
    private Integer portNo;

    public MessageTransmitter(String hostname,
                              String message,
                              Integer portNo) {
        this.hostname = hostname;
        this.message = message;
        this.portNo = portNo;
    }

    public MessageTransmitter() {
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket(hostname, portNo);
            socket.getOutputStream().write(message.getBytes());
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
